# sample-pipeline

Simple Python library class for managing books. Tools used include:

- Gitlab, for code hosting, version control, and CI/CD
- _behave_, Python library for Cucumber behavioural testing
- _setuptools_, Python packaging library
- SonarQube, for code quality analysis
- Ansible
- Docker