Feature: The library for managing books

  Scenario: Add book
    Given an empty library
    When book 12345 is asked to be added to the library
    Then the library has only book 12345

  Scenario: Remove book
    Given a library with only book 12345
    When book 12345 is asked to be removed from the library
    Then the library is empty

  Scenario: Remove nonexistant book
    Given an empty library
    When book 12345 is asked to be removed from the library
    Then an appropriate error is raised
