import behave
import librarypackage as LP

@given("an empty library")
def step_impl(context):
    context.l = LP.Library([])

@when("book 12345 is asked to be added to the library")
def step_impl(context):
    context.b = LP.Book(12345, "BookName", "AuthorName", 1998)
    context.l.add_book(context.b)

@then("the library has only book 12345")
def step_impl(context):
    assert context.l.books == [context.b]

@given("a library with only book 12345")
def step_impl(context):
    context.b = LP.Book(12345, "BookName", "AuthorName", 1998)
    context.l = LP.Library([context.b])

@when("book 12345 is asked to be removed from the library")
def step_impl(context):
    try:
        context.l.remove_book(12345)
    except KeyError as e:
        context.errorRaised = e

@then("the library is empty")
def step_impl(context):
    assert not context.l.books

@then("an appropriate error is raised")
def step_impl(context):
    assert str(context.errorRaised) == "'Book id not present'"