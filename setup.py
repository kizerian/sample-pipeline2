import setuptools

with open("README.md", "r") as fh:
    description = fh.read()

setuptools.setup(
    name="librarypackage",
    author="Shaams Dally",
    description=description,
    packages=["librarypackage"],
    package_dir={"librarypackage":"src/librarypackage"}
)
