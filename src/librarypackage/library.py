class Library:

    # books is an array of book objects
    def __init__(self, books):
        self.books = books
    
    def add_book(self, book):
        if book.id in [b.id for b in self.books]:
            raise KeyError("Book id already present")
        self.books.append(book)
    
    def remove_book(self, book_id):
        if book_id not in [b.id for b in self.books]:
            raise KeyError("Book id not present")
        self.books = [b for b in self.books if b.id != book_id]
